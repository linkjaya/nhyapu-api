package com.nhyapu.email.domain;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "emailTemplate", "emailRecepient", "emailMessageKeyValues" })
public class EmailRequest {

	@JsonProperty("emailTemplate")
	@NotNull
	private String emailTemplate;

	@JsonProperty("emailRecepient")
	@NotNull
	private List<String> emailRecepient = null;

	@JsonProperty("emailMessageKeyValues")
	private List<EmailMessageKeyValue> emailMessageKeyValues = null;

	@JsonProperty("emailTemplate")
	public String getEmailTemplate() {
		return emailTemplate;
	}

	@JsonProperty("emailTemplate")
	public void setEmailTemplate(String emailTemplate) {
		this.emailTemplate = emailTemplate;
	}

	@JsonProperty("emailRecepient")
	public List<String> getEmailRecepient() {
		return emailRecepient;
	}

	@JsonProperty("emailRecepient")
	public void setEmailRecepient(List<String> emailRecepient) {
		this.emailRecepient = emailRecepient;
	}

	@JsonProperty("emailMessageKeyValues")
	public List<EmailMessageKeyValue> getEmailMessageKeyValues() {
		return emailMessageKeyValues;
	}

	@JsonProperty("emailMessageKeyValues")
	public void setEmailMessageKeyValues(List<EmailMessageKeyValue> emailMessageKeyValues) {
		this.emailMessageKeyValues = emailMessageKeyValues;
	}

}
