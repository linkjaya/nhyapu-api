package com.nhyapu.email.domain;

public enum EmailTemplate {

	HELLO("HELLO", "Hello World!!"),
	WELCOME("welcome", "Welcome to Nhyapu");

	private String name;
	private String subject;



	private EmailTemplate(String name, String subject) {
		this.name = name;
		this.subject = subject;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

}
