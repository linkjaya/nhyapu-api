package com.nhyapu.email.service;

import java.util.List;

import com.nhyapu.authentication.model.Account;

public interface EmailService {
	
	public void sendWelcomeEmail(Account account, List<String> lotteries);
	

}
