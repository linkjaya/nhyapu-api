package com.nhyapu.email.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import com.nhyapu.authentication.model.Account;
import com.nhyapu.email.domain.EmailMessageKeyValue;
import com.nhyapu.email.domain.EmailRequest;
import com.nhyapu.email.domain.EmailResponse;
import com.nhyapu.email.domain.EmailTemplate;

@Service
public class EmailServiceImpl implements EmailService {

	@Value("${nhyapu.email.url}")
	String emailUrl;

	@Autowired
	private RestOperations restTemplate;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public void sendWelcomeEmail(Account account, List<String> lotteries) {

		EmailRequest emailRequest = new EmailRequest();
		List<EmailMessageKeyValue> emailMessageKeyValues = new ArrayList<EmailMessageKeyValue>();
		emailRequest.setEmailTemplate(EmailTemplate.WELCOME.getName());
		emailRequest.setEmailRecepient(Arrays.asList(account.getUserName()));

		emailMessageKeyValues = lotteries.stream().map(lottery -> new EmailMessageKeyValue("lottery", lottery))
				.collect(Collectors.toList());

		emailMessageKeyValues.add(new EmailMessageKeyValue("name", account.getFirstName()));

		emailRequest.setEmailMessageKeyValues(emailMessageKeyValues);
		HttpEntity<EmailRequest> request = new HttpEntity<>(emailRequest);

		ResponseEntity<EmailResponse> response = restTemplate.exchange(emailUrl, HttpMethod.POST, request,
				EmailResponse.class);

		if (response.getStatusCode() == HttpStatus.OK) {
			log.info("Welcome Message queued Successfully");
		} else {
			log.error("Something wrong during sending welcome email");
		}

	}

}
