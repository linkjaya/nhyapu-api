package com.nhyapu.authentication.controller;

import java.security.Principal;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nhyapu.authentication.model.Account;
import com.nhyapu.authentication.service.AccountService;
import com.nhyapu.lottery.model.RegistrationResponse;
import com.nhyapu.settings.email.SmtpMailSender;
import com.nhyapu.settings.errors.InvalidRequestException;

/**
 * This controller is responsible to manage the authentication system. Login -
 * Register - Forgot password - Account Confirmation
 */
@RestController
public class AuthenticationController {

	@Autowired
	protected AuthenticationManager authenticationManager;

	@Autowired
	private AccountService accountService;

	@Autowired
	private SmtpMailSender smtpMailSender;

	// @Autowired
	// TokenGenerator tokenGenerator;

	@RequestMapping(value = "/all/sample", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Account> sampleGet(HttpServletResponse response) {
		return new ResponseEntity<Account>(accountService.findByUsername("jaya@nhyapu.com"), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/all/sample", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Account> sample(HttpServletResponse response) {
		return new ResponseEntity<Account>(accountService.findByUsername("admin@nhyapu.com"), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/admin/sample", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Account> sampleAdmin(HttpServletResponse response) {
		return new ResponseEntity<Account>(accountService.findByUsername("jaya@nhyapu.com"), HttpStatus.CREATED);
	}

	/**
	 * Create a new user account
	 * 
	 * @param account
	 *            user account
	 * @return created account as json
	 */
	@RequestMapping(value = "/all/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RegistrationResponse> register(@Valid @RequestBody Account account, BindingResult errors,
			Principal principal) {

		// Check if account is unique
		if (errors.hasErrors()) {
			throw new InvalidRequestException("Username already exists", errors);
		}

		Account createdAccount = accountService.createNewAccount(account);

		return new ResponseEntity<RegistrationResponse>(new RegistrationResponse(createdAccount, null),
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/forgot-password", method = RequestMethod.GET)
	public ResponseEntity<String> forgotPassword() throws MessagingException {
		String response = "{success: true}";
		smtpMailSender.send("cpapidas@gmail.com", "Password forgot", "Forgot password url");
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}

}
