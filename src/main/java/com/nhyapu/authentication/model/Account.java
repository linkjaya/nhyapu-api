package com.nhyapu.authentication.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nhyapu.lottery.model.LotteryPoolParticipant;
import com.nhyapu.settings.validators.UniqueUsername;


/**
 * The persistent class for the Account database table.
 * 
 */
@Entity
@Table(name = "Account")
@NamedQuery(name="Account.findAll", query="SELECT a FROM Account a")
public class Account implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="account_id")
	private Long accountId;

	@Temporal(TemporalType.DATE)
	@Column(name="create_date")
	@JsonIgnore
	private Date createDate;

	@Column(name="created_by")
	@JsonIgnore
	private String createdBy;

	@Column(name="credentials_expired")
	@NotNull
	private boolean credentialsExpired;

	@NotNull
	private boolean enabled;

	@NotNull
	private boolean expired;

	@Column(name="first_name")
	@NotNull(message="First Name can not be null")
	private String firstName;

	@Column(name="last_name")
	private String lastName;

	@NotNull
	private boolean locked;

	@NotNull
    @Size(min = 8, max = 255, message = "Password have to be grater than 8 characters")
	private String password;

	@NotNull(message = "Phone number can not be null")
	private BigInteger phone;

	@Temporal(TemporalType.DATE)
	@Column(name="update_date")
	@JsonIgnore
	private Date updateDate;

	@Column(name="updated_by")
	@JsonIgnore
	private String updatedBy;

	@Column(name="user_name", unique = true)
	@UniqueUsername(message="Username already exists")
    @Size(min = 8, max = 255, message = "Username have to be grater than 8 characters")
    private String userName;

	//bi-directional many-to-one association to Account_Role
	@OneToMany(fetch = FetchType.EAGER, mappedBy="account")
	private Set<AccountRole> accountRoles = new HashSet<AccountRole>();

	//bi-directional many-to-one association to Lottery_Pool_Participant
	@OneToMany(mappedBy="account")
	private Set<LotteryPoolParticipant> lotteryPoolParticipants = new HashSet<LotteryPoolParticipant>();

	public Account() {
	}
	
	public Account(Account account){
        this.accountId = account.getAccountId();
        this.userName = account.getUserName();
        this.password = account.getPassword();
        this.enabled = account.isEnabled();
        this.credentialsExpired = account.isCredentialsExpired();
        this.locked = account.isExpired();
        this.firstName = account.getFirstName();
        this.lastName=account.getLastName();
        this.phone=account.getPhone();
        this.accountRoles = account.getAccountRoles();
        this.lotteryPoolParticipants = account.getLotteryPoolParticipants();
    }

	public Long getAccountId() {
		return this.accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public boolean isCredentialsExpired() {
		return this.credentialsExpired;
	}

	public void setCredentialsExpired(boolean credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isExpired() {
		return this.expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isLocked() {
		return this.locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BigInteger getPhone() {
		return this.phone;
	}

	public void setPhone(BigInteger phone) {
		this.phone = phone;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Set<AccountRole> getAccountRoles() {
		return this.accountRoles;
	}

	public void setAccountRoles(Set<AccountRole> accountRoles) {
		this.accountRoles = accountRoles;
	}

	public AccountRole addAccountRole(AccountRole accountRole) {
		getAccountRoles().add(accountRole);
		accountRole.setAccount(this);

		return accountRole;
	}

	public AccountRole removeAccountRole(AccountRole accountRole) {
		getAccountRoles().remove(accountRole);
		accountRole.setAccount(null);

		return accountRole;
	}

	public Set<LotteryPoolParticipant> getLotteryPoolParticipants() {
		return this.lotteryPoolParticipants;
	}

	public void setLotteryPoolParticipants(Set<LotteryPoolParticipant> lotteryPoolParticipants) {
		this.lotteryPoolParticipants = lotteryPoolParticipants;
	}

	public LotteryPoolParticipant addLotteryPoolParticipant(LotteryPoolParticipant lotteryPoolParticipant) {
		getLotteryPoolParticipants().add(lotteryPoolParticipant);
		lotteryPoolParticipant.setAccount(this);

		return lotteryPoolParticipant;
	}

	public LotteryPoolParticipant removeLotteryPoolParticipant(LotteryPoolParticipant lotteryPoolParticipant) {
		getLotteryPoolParticipants().remove(lotteryPoolParticipant);
		lotteryPoolParticipant.setAccount(null);

		return lotteryPoolParticipant;
	}

}