package com.nhyapu.authentication.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the Account_Role database table.
 * 
 */
@Entity
@Table(name = "Account_Role")
@NamedQuery(name="AccountRole.findAll", query="SELECT a FROM AccountRole a")
public class AccountRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="account_role_id")
	private Long accountRoleId;

	@Temporal(TemporalType.DATE)
	@Column(name="create_date")
	@NotNull(message = "Create Date can not be null")
	@JsonIgnore
	private Date createDate;

	@Column(name="created_by")
	@NotNull(message = "Created By can not be null")
	@JsonIgnore
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="update_date")
	@JsonIgnore
	private Date updateDate;

	@Column(name="updated_by")
	@JsonIgnore
	private String updatedBy;

	//bi-directional many-to-one association to Role
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="role_id")
	private Role role;

	//bi-directional many-to-one association to Account
	@ManyToOne
	@JoinColumn(name="account_id")
	private Account account;

	public AccountRole() {
	}

	public Long getAccountRoleId() {
		return this.accountRoleId;
	}

	public void setAccountRoleId(Long accountRoleId) {
		this.accountRoleId = accountRoleId;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

}