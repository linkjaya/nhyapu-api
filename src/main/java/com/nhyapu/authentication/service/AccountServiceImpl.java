package com.nhyapu.authentication.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nhyapu.authentication.model.Account;
import com.nhyapu.authentication.model.AccountRole;
import com.nhyapu.authentication.model.Role;
import com.nhyapu.email.service.EmailService;
import com.nhyapu.lottery.lotterypool.service.LotteryTypeService;
import com.nhyapu.lottery.repository.AccountRepository;
import com.nhyapu.lottery.repository.AccountRoleRepository;


@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AccountServiceImpl implements AccountService {

    /**
     * The Logger for this class.
     */
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * The Spring Data repository for Account entities.
     */
    @Autowired
    private AccountRepository accountRepository;
    
    @Autowired
    private AccountRoleRepository accountRoleRepository;
    
    
	@Autowired
	private EmailService emailService;
	
	@Autowired
	LotteryTypeService lotteryTypeService;

    /**
     * The Spring Data repository for Role entities
     */
    @Autowired
    private RoleService roleService;

    /**
     * Find and return all accounts
     * @return collection of all accounts
     */
    @Override
    public Collection<Account> findAll() {
        Collection<Account> accounts = accountRepository.findAll();
        return accounts;
    }

    /**
     * Find user by username
     * @param username the username of the user
     * @return the user account
     */
    @Override
    public Account findByUsername(String username) {
        Account account = accountRepository.findByUsername(username);
        return account;
    }

    /**
     * Create a new user as simple user. Find the simple user role from the database
     * add assign to the many to many collection
     * @param account - new Account of user
     * @return - the created account
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Account createNewAccount(Account account) {

    	AccountRole accountRole = new AccountRole();
    	
        // Add the simple user role
        Role role = roleService.findByCode("ROLE_USER");
        
        
        // Validate the password
        if (account.getPassword().length() < 8){
            throw new EntityExistsException("password should be greater than 8 characters");
        }

        // Encode the password
        account.setPassword(new BCryptPasswordEncoder().encode(account.getPassword()));

        // Create the role
        Date currentDate = new Date();
        String createdBy = "SERVICE";
        
        accountRole.setCreateDate(currentDate);
        accountRole.setCreatedBy(createdBy);
        
        account.addAccountRole(accountRole);
        account.setCreateDate(currentDate);
        account.setCreatedBy(createdBy);
        
        accountRole.setAccount(account);
        accountRole.setRole(role);
        
       
        account= accountRepository.save(account);
        accountRoleRepository.save(accountRole);
        
        List<String> lotteries = lotteryTypeService.findAll().stream().map(lotteryType -> lotteryType.getDescription()).collect(Collectors.toList());
    	
		emailService.sendWelcomeEmail(account, lotteries);
		
		logger.info("Account Created");
        
        return account;
        
        
    }
}
