package com.nhyapu.authentication.service;

import java.util.Collection;

import com.nhyapu.authentication.model.Account;

/**
 * Created by christospapidas on 24012016--.
 */
public interface AccountService {

    Collection<Account> findAll();

    Account findByUsername(String userename);

    Account createNewAccount(Account account);


}
