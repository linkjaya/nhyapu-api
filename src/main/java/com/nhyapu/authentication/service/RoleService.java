package com.nhyapu.authentication.service;

import com.nhyapu.authentication.model.Role;

/**
 * Created by christospapidas on 25012016--.
 */
public interface RoleService {

    Role findById(Long id);

    Role findByCode(String code);

}
