package com.nhyapu.security.helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerEndpointsConfiguration;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.stereotype.Component;

import com.nhyapu.authentication.model.Role;

@Component
public class TokenGenerator {
	
	@Autowired
	private AuthorizationServerEndpointsConfiguration configuration;

	public OAuth2AccessToken generateOAuth2AccessToken(String userName, String password, Set<Role> roles, List<String> scopes) {

	    Map<String, String> requestParameters = new HashMap<String, String>();
	    Map<String, Serializable> extensionProperties = new HashMap<String, Serializable>();

	    boolean approved = true;
	    Set<String> responseTypes = new HashSet<String>();
	    responseTypes.add("code");

	    // Authorities
	    List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
	    for(Role role: roles)
	        authorities.add(new SimpleGrantedAuthority(role.getCode()));

	    OAuth2Request oauth2Request = new OAuth2Request(requestParameters, "clientapp", authorities, approved, new HashSet<String>(scopes), new HashSet<String>(Arrays.asList("resourceIdTest")), null, responseTypes, extensionProperties);

	    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userName, password, authorities);

	    OAuth2Authentication auth = new OAuth2Authentication(oauth2Request, authenticationToken);
	    
	    
	    
	    AuthorizationServerTokenServices tokenService = configuration.getEndpointsConfigurer().getTokenServices();
	    

	    OAuth2AccessToken token = tokenService.createAccessToken(auth);
	    System.out.println(token.getRefreshToken());

	    return token;
	}

}
