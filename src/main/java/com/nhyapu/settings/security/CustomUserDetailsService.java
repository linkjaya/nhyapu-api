

package com.nhyapu.settings.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.nhyapu.authentication.model.Account;
import com.nhyapu.authentication.model.AccountRole;
import com.nhyapu.authentication.model.Role;
import com.nhyapu.lottery.repository.AccountRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final AccountRepository userRepository;

    @Autowired
    public CustomUserDetailsService(AccountRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Account account = userRepository.findByUsername(username);

        if (account == null) {
            // Not found...
            throw new UsernameNotFoundException(
                    "User " + username + " not found.");
        }
        
        if(account.getAccountRoles()==null || account.getAccountRoles().isEmpty())
        {

        //if (account.getRoles() == null || account.getRoles().isEmpty()) {
            // No Roles assigned to user...
            throw new UsernameNotFoundException("User not authorized.");
        }


        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        Role role;
        for (AccountRole accountRole : account.getAccountRoles()) {
        	role = accountRole.getRole();
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getCode()));
        }

        User userDetails = new User(account.getUserName(),
                account.getPassword(), account.isEnabled(),
                !account.isExpired(), !account.isCredentialsExpired(),
                !account.isLocked(), grantedAuthorities);

        return userDetails;
    }

    private final static class UserRepositoryUserDetails extends Account implements UserDetails {

        private static final long serialVersionUID = 1L;



        private UserRepositoryUserDetails(Account user) {
            super(user);
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
            Role role; 
            for(AccountRole accountRole: getAccountRoles())
            {
            	role = accountRole.getRole();
            	grantedAuthorities.add(new SimpleGrantedAuthority(role.getCode()));
            	
            }
                 
            return grantedAuthorities;
        }

        @Override
        public String getUsername() {
            return getUsername();
        }

        @Override
        public boolean isAccountNonExpired() {
            return !isExpired();
        }

        @Override
        public boolean isAccountNonLocked() {
            return !isLocked();
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return !isCredentialsExpired();
        }

        @Override
        public boolean isEnabled() {
            return isEnabled();
        }

        @Override
        public Set<AccountRole> getAccountRoles() {
            return getAccountRoles();
        }
    }
}
