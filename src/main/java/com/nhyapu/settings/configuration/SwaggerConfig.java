package com.nhyapu.settings.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {                
	    return new Docket(DocumentationType.SWAGGER_2)          
	      .select()
	      .apis(RequestHandlerSelectors.basePackage("com.nhyapu"))
	      .paths(getPaths())
	      .build()
	      .apiInfo(apiInfo());
	}
	 
	private ApiInfo apiInfo() {
		
	    ApiInfo apiInfo = new ApiInfo(
	      "My REST API",
	      "Some custom description of API.",
	      "API TOS",
	      "Terms of service",
	      new Contact("Nhyapu", "www.nhyapu.com", "info@nhyapu.com"),
	     "License of API",
	      "API license URL");
	    return apiInfo;
	}
	
	private Predicate<String> getPaths()
	{
		//{PathSelectors.ant("/admin/**"), PathSelectors.ant("/all/**"), PathSelectors.ant("/user/**"), PathSelectors.ant("/finance/**")};
		List<Predicate<String>> predicateList  = new ArrayList<Predicate<String>>();
		predicateList.add(PathSelectors.ant("/admin/**"));
		predicateList.add(PathSelectors.ant("/all/**"));
		predicateList.add(PathSelectors.ant("/user/**"));
		predicateList.add(PathSelectors.ant("/finance/**"));
		Predicate<String> predicate = Predicates.or(predicateList);
		return predicate;
		
		
	}

}