package com.nhyapu.settings.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

/**
 * Wres up environment properties to be used with ${} placeholders,
 * and business properties to be uused with #{}
 * <p>
 * 
 */
@Configuration
@PropertySources({
    @PropertySource("classpath:/properties/local.properties"),
    @PropertySource("classpath:/properties/common.properties")
})
public class PropertiesConfig {
	
	private static final String BUSINESS_PROPERTIES = 
			"/properties/business.properties";
	
	@Autowired
	Environment environment;
	
	
	/**
	 * Exposes the common.properties file as a Properties object in the
	 * context. 
	 * To be used with @Value("#{businessProperties.EXAMPLE_PROP}")
	 * 
	 * @return
	 */
	@Bean
	public PropertiesFactoryBean businessProperties() {
		PropertiesFactoryBean factoryBean = new PropertiesFactoryBean();
		
		factoryBean.setLocation(new ClassPathResource(BUSINESS_PROPERTIES));
		
		return factoryBean;
	}
	
	
	/**
	 * Configures the environment properties (lcl.properties, prod.properties)
	 * to be used with placeholders in @Value
	 * Example: @Value("${esbEndpoint}")
	 * 
	 * @return
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
		
	    return configurer;
	}
}
