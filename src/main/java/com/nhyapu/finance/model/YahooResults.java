package com.nhyapu.finance.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class YahooResults {
	
	@JsonProperty("query")
	public QuoteList quoteList;

}
