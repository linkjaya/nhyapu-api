package com.nhyapu.finance.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteList {
	
	@JsonProperty("query")
	public Map<String, Object> query;
	
	public List<Quote> getQuotes() {
		
		Map<String, List> results = (Map<String, List>) query.get("results");
		List<Map<String, String>> quoteList = (List<Map<String, String>>)results.get("quote");
		
		List<Quote> quotes = new ArrayList<Quote>();
		final ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
		
		
		for(Map<String, String> quoteMap: quoteList)
		{
			 Quote quote = mapper.convertValue(quoteMap, Quote.class);
			 quotes.add(quote);
		}
		
		
		
		return quotes;
		
	}
	
	

}
