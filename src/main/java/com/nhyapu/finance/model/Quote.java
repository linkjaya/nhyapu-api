package com.nhyapu.finance.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Quote {
	
	@JsonProperty("symbol")
	private String symbol;

	@JsonProperty("Ask")
	private String ask;

	@JsonProperty("AverageDailyVolume")
	private String averageDailyVolume;
	
	@JsonProperty("Bid")
	private String bid;
	
	@JsonProperty("AskRealtime")
	private String askRealtime;
	
	@JsonProperty("BidRealtime")
	private String bidRealtime;

	
	@JsonProperty("BookValue")
	private String bookValue;
	
	@JsonProperty("Change_PercentChange")
	private String changePercentChange;
	
	@JsonProperty("Change")
	private String change;
	
	@JsonProperty("Commission")
	private String commission;
	
	@JsonProperty("Currency")
	private String currency;
	
	@JsonProperty("ChangeRealtime")
	private String ChangeRealtime;
	
	@JsonProperty("AfterHoursChangeRealtime")
	private String afterHoursChangeRealtime;

	@JsonProperty("DividendShare")
	private String dividendShare;

	@JsonProperty("LastTradeDate")
	private String lastTradeDate;
	
	@JsonProperty("TradeDate")
	private String tradeDate;
	
	@JsonProperty("EarningsShare")
	private String earningsShare;
	
	
	@JsonProperty("ErrorIndicationreturnedforsymbolchangedinvalid")
	private String errorIndicationreturnedforsymbolchangedinvalid;

	@JsonProperty("EPSEstimateCurrentYear")
	private String epsEstimateCurrentYear;
	
	@JsonProperty("EPSEstimateNextYear")
	private String epsEstimateNextYear;
	
	@JsonProperty("EPSEstimateNextQuarter")
	private String epsEstimateNextQuarter;
	
	@JsonProperty("DaysLow")
	private String daysLow;
	
	@JsonProperty("DaysHigh")
	private String daysHigh;
	
	@JsonProperty("YearLow")
	private String yearLow;
	
	@JsonProperty("YearHigh")
	private String yearHigh;
	
	@JsonProperty("HoldingsGainPercent")
	private String holdingsGainPercent;
	
	@JsonProperty("AnnualizedGain")
	private String annualizedGain;

	@JsonProperty("HoldingsGain")
	private String holdingsGain;

	@JsonProperty("HoldingsGainPercentRealtime")
	private String holdingsGainPercentRealtime;

	
	@JsonProperty("HoldingsGainRealtime")
	private String holdingsGainRealtime;
	
	@JsonProperty("MoreInfo")
	private String moreInfo;
	
	@JsonProperty("OrderBookRealtime")
	private String orderBookRealtime;
	
	@JsonProperty("MarketCapitalization")
	private String marketCapitalization;
	
	@JsonProperty("MarketCapRealtime")
	private String marketCapRealtime;
	
	@JsonProperty("EBITDA")
	private String ebitda;
	
	@JsonProperty("ChangeFromYearLow")
	private String changeFromYearLow;
	
	@JsonProperty("PercentChangeFromYearLow")
	private String percentChangeFromYearLow;
	
	@JsonProperty("LastTradeRealtimeWithTime")
	private String lastTradeRealtimeWithTime;
	
	@JsonProperty("ChangePercentRealtime")
	private String changePercentRealtime;
	
	@JsonProperty("ChangeFromYearHigh")
	private String changeFromYearHigh;
	
	@JsonProperty("PercebtChangeFromYearHigh")
	private String percebtChangeFromYearHigh;
	
	@JsonProperty("LastTradeWithTime")
	private String lastTradeWithTime;
	
	@JsonProperty("LastTradePriceOnly")
	private String lastTradePriceOnly;
	
	@JsonProperty("HighLimit")
	private String highLimit;
	
	@JsonProperty("LowLimit")
	private String lowLimit;
	
	@JsonProperty("DaysRange")
	private String daysRange;
	
	@JsonProperty("DaysRangeRealtime")
	private String daysRangeRealtime;
	
	@JsonProperty("FiftydayMovingAverage")
	private String fiftydayMovingAverage;
	
	@JsonProperty("TwoHundreddayMovingAverage")
	private String twoHundreddayMovingAverage;
	
	@JsonProperty("ChangeFromTwoHundreddayMovingAverage")
	private String changeFromTwoHundreddayMovingAverage;
	
	@JsonProperty("PercentChangeFromTwoHundreddayMovingAverage")
	private String percentChangeFromTwoHundreddayMovingAverage;
	
	@JsonProperty("ChangeFromFiftydayMovingAverage")
	private String changeFromFiftydayMovingAverage;
	
	@JsonProperty("PercentChangeFromFiftydayMovingAverage")
	private String percentChangeFromFiftydayMovingAverage;
	
	@JsonProperty("Name")
	private String name;
	
	@JsonProperty("Notes")
	private String notes;
	
	@JsonProperty("Open")
	private String open;
	
	@JsonProperty("PreviousClose")
	private String previousClose;
	
	@JsonProperty("PricePaid")
	private String pricePaid;
	
	@JsonProperty("ChangeinPercent")
	private String changeinPercent;
	
	@JsonProperty("PriceSales")
	private String priceSales;
	
	@JsonProperty("PriceBook")
	private String priceBook;
	
	@JsonProperty("ExDividendDate")
	private String exDividendDate;
	
	@JsonProperty("PERatio")
	private String peRatio;
	
	@JsonProperty("DividendPayDate")
	private String dividendPayDate;
	
	@JsonProperty("PERatioRealtime")
	private String peRatioRealtime;
	
	@JsonProperty("PEGRatio")
	private String pegRatio;
	
	@JsonProperty("PriceEPSEstimateCurrentYear")
	private String priceEPSEstimateCurrentYear;
	
	@JsonProperty("PriceEPSEstimateNextYear")
	private String priceEPSEstimateNextYear;
	
	//@JsonProperty("Symbol")
	//private String symbol;
	
	@JsonProperty("SharesOwned")
	private String sharesOwned;
	
	
	@JsonProperty("ShortRatio")
	private String shortRatio;
	
	@JsonProperty("LastTradeTime")
	private String lastTradeTime;
	
	@JsonProperty("TickerTrend")
	private String tickerTrend;
	
	@JsonProperty("OneyrTargetPrice")
	private String oneyrTargetPrice;
	
	@JsonProperty("Volume")
	private String volume;
	
	@JsonProperty("HoldingsValue")
	private String holdingsValue;

	
	@JsonProperty("HoldingsValueRealtime")
	private String holdingsValueRealtime;
	
	@JsonProperty("YearRange")
	private String yearRange;
	
	@JsonProperty("DaysValueChange")
	private String daysValueChange;
	
	@JsonProperty("DaysValueChangeRealtime")
    private String daysValueChangeRealtime;
	
	@JsonProperty("StockExchange")
	private String stockExchange;
	
	@JsonProperty("DividendYield")
	private String dividendYield;
	
	@JsonProperty("PercentChange")
	private String percentChange;




}
