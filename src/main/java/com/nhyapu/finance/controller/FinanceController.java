package com.nhyapu.finance.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import com.nhyapu.finance.model.Quote;
import com.nhyapu.finance.repository.QuoteDetailRepository;

@Controller
@RequestMapping(value = "/finance")
public class FinanceController {

	@Value("${yahoo.getstockdetail.url}")
	private String yahooGetStockDetailUrl;

	@Autowired
	private QuoteDetailRepository quoteDetailRepository;

	@RequestMapping(value = "/{stockSymbol}", method = RequestMethod.GET)
	@ResponseBody
	public Quote getByStockSymbol(
			@PathVariable("stockSymbol") final String stockSymbol,
			final UriComponentsBuilder uriBuilder,
			final HttpServletResponse response) {

		List<String> symbols = new ArrayList<String>();
		symbols.add("CERN");
		return quoteDetailRepository.getQuoteDetails(symbols).get(0);
	}

}
