package com.nhyapu.finance.repository;

import java.util.List;

import com.nhyapu.finance.model.Quote;

public interface QuoteDetailRepository {
	
	public List<Quote> getQuoteDetails(List<String>symbols);

}
