package com.nhyapu.finance.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.nhyapu.finance.model.Quote;
import com.nhyapu.finance.model.QuoteList;

@Repository
public class YahooQuoteDetailRepository implements QuoteDetailRepository {

	@Value("${yahoo.getstockdetail.url}")
	private String yahooGetStockDetailUrl;
	

	
	@Override
	public List<Quote> getQuoteDetails(List<String> symbols) {
		SimpleClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory();
		
		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
		ResponseEntity<QuoteList> response = restTemplate.exchange(
				yahooGetStockDetailUrl, HttpMethod.GET, null,
				QuoteList.class);
		List<Quote> quoteList = null;
		quoteList= response.getBody().getQuotes();
		return quoteList;
	}}
