package com.nhyapu.lottery.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;


/**
 * The persistent class for the Lottery database table.
 * 
 */
@Entity
@Table(name = "Lottery")
@NamedQuery(name="Lottery.findAll", query="SELECT l FROM Lottery l")
public class Lottery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="lottery_id")
	private Long lotteryId;

	@Temporal(TemporalType.DATE)
	@Column(name="create_date")
	@NotNull
	private Date createDate;

	@Column(name="created_by")
	@NotNull
	private String createdBy;

	@NotNull
	private Integer number1;

	private Integer number10;

	@NotNull
	private Integer number2;

	@NotNull
	private Integer number3;

	@NotNull
	private Integer number4;

	@NotNull
	private Integer number5;

	private Integer number6;

	private Integer number7;

	private Integer number8;

	private Integer number9;

	@Temporal(TemporalType.DATE)
	@Column(name="update_date")
	private Date updateDate;

	@Column(name="updated_by")
	private String updatedBy;

	//bi-directional many-to-one association to Lottery_Pool
	@ManyToOne
	@JoinColumn(name="lottery_pool_id")
	@JsonBackReference
	private LotteryPool lotteryPool;

	public Lottery() {
	}

	public Long getLotteryId() {
		return this.lotteryId;
	}

	public void setLotteryId(Long lotteryId) {
		this.lotteryId = lotteryId;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getNumber1() {
		return this.number1;
	}

	public void setNumber1(Integer number1) {
		this.number1 = number1;
	}

	public Integer getNumber10() {
		return this.number10;
	}

	public void setNumber10(Integer number10) {
		this.number10 = number10;
	}

	public Integer getNumber2() {
		return this.number2;
	}

	public void setNumber2(Integer number2) {
		this.number2 = number2;
	}

	public Integer getNumber3() {
		return this.number3;
	}

	public void setNumber3(Integer number3) {
		this.number3 = number3;
	}

	public Integer getNumber4() {
		return this.number4;
	}

	public void setNumber4(Integer number4) {
		this.number4 = number4;
	}

	public Integer getNumber5() {
		return this.number5;
	}

	public void setNumber5(Integer number5) {
		this.number5 = number5;
	}

	public Integer getNumber6() {
		return this.number6;
	}

	public void setNumber6(Integer number6) {
		this.number6 = number6;
	}

	public Integer getNumber7() {
		return this.number7;
	}

	public void setNumber7(Integer number7) {
		this.number7 = number7;
	}

	public Integer getNumber8() {
		return this.number8;
	}

	public void setNumber8(Integer number8) {
		this.number8 = number8;
	}

	public Integer getNumber9() {
		return this.number9;
	}

	public void setNumber9(Integer number9) {
		this.number9 = number9;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LotteryPool getLotteryPool() {
		return this.lotteryPool;
	}

	public void setLotteryPool(LotteryPool lotteryPool) {
		this.lotteryPool = lotteryPool;
	}

}