package com.nhyapu.lottery.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the Lottery_Pool database table.
 * 
 */
@Entity
@Table(name = "Lottery_Pool")
@NamedQuery(name="LotteryPool.findAll", query="SELECT l FROM LotteryPool l")
public class LotteryPool implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="lottery_pool_id")
	private Long lotteryPoolId;

	@Temporal(TemporalType.DATE)
	@Column(name="create_date")
	@NotNull
	private Date createDate;

	@Column(name="created_by")
	@NotNull
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="lottery_draw_date")
	@NotNull
	private Date lotteryDrawDate;

	@Column(name="lottery_pool_size")
	@NotNull
	private int lotteryPoolSize;

	@Column(name="participant_size")
	@NotNull
	private int participantSize;

	@Temporal(TemporalType.DATE)
	@Column(name="update_date")
	private Date updateDate;

	@Column(name="updated_by")
	private String updatedBy;
	
	@Column(name="lottery_pool_type")
	@NotNull
	private String lotteryPoolType;

	//bi-directional many-to-one association to Lottery
	@OneToMany(mappedBy="lotteryPool")
    @JsonManagedReference
	private Set<Lottery> lotteries = new HashSet<Lottery>();

	//bi-directional many-to-one association to Lottery_Type
	@ManyToOne
	@JoinColumn(name="lottery_type_id")
	@NotNull
	private LotteryType lotteryType;

	//bi-directional many-to-one association to Lottery_Pool_Participant
	@OneToMany(mappedBy="lotteryPool", fetch=FetchType.EAGER)
	private Set<LotteryPoolParticipant> lotteryPoolParticipants = new HashSet<LotteryPoolParticipant>();

	public LotteryPool() {
	}

	public Long getLotteryPoolId() {
		return this.lotteryPoolId;
	}

	public void setLotteryPoolId(Long lotteryPoolId) {
		this.lotteryPoolId = lotteryPoolId;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getLotteryPoolType() {
		return lotteryPoolType;
	}

	public void setLotteryPoolType(String lotteryPoolType) {
		this.lotteryPoolType = lotteryPoolType;
	}

	public Date getLotteryDrawDate() {
		return this.lotteryDrawDate;
	}

	public void setLotteryDrawDate(Date lotteryDrawDate) {
		this.lotteryDrawDate = lotteryDrawDate;
	}

	public int getLotteryPoolSize() {
		return this.lotteryPoolSize;
	}

	public void setLotteryPoolSize(int lotteryPoolSize) {
		this.lotteryPoolSize = lotteryPoolSize;
	}

	public int getParticipantSize() {
		return this.participantSize;
	}

	public void setParticipantSize(int participantSize) {
		this.participantSize = participantSize;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Set<Lottery> getLotteries() {
		return this.lotteries;
	}

	public void setLotteries(Set<Lottery> lotteries) {
		this.lotteries = lotteries;
	}

	public Lottery addLottery(Lottery lottery) {
		getLotteries().add(lottery);
		lottery.setLotteryPool(this);

		return lottery;
	}

	public Lottery removeLottery(Lottery lottery) {
		getLotteries().remove(lottery);
		lottery.setLotteryPool(null);

		return lottery;
	}

	public LotteryType getLotteryType() {
		return this.lotteryType;
	}

	public void setLotteryType(LotteryType lotteryType) {
		this.lotteryType = lotteryType;
	}

	public Set<LotteryPoolParticipant> getLotteryPoolParticipants() {
		return this.lotteryPoolParticipants;
	}

	public void setLotteryPoolParticipants(Set<LotteryPoolParticipant> lotteryPoolParticipants) {
		this.lotteryPoolParticipants = lotteryPoolParticipants;
	}

	public LotteryPoolParticipant addLotteryPoolParticipant(LotteryPoolParticipant lotteryPoolParticipant) {
		getLotteryPoolParticipants().add(lotteryPoolParticipant);
		lotteryPoolParticipant.setLotteryPool(this);

		return lotteryPoolParticipant;
	}

	public LotteryPoolParticipant removeLotteryPoolParticipant(LotteryPoolParticipant lotteryPoolParticipant) {
		getLotteryPoolParticipants().remove(lotteryPoolParticipant);
		lotteryPoolParticipant.setLotteryPool(null);

		return lotteryPoolParticipant;
	}

}