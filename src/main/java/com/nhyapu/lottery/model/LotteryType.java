package com.nhyapu.lottery.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


/**
 * The persistent class for the Lottery_Type database table.
 * 
 */
@Entity
@Table(name = "Lottery_Type")
@NamedQuery(name="LotteryType.findAll", query="SELECT l FROM LotteryType l")
public class LotteryType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="lottery_type_id")
	private int lotteryTypeId;

	@Temporal(TemporalType.DATE)
	@Column(name="create_date")
	@NotNull
	private Date createDate;

	@Column(name="created_by")
	@NotNull
	private String createdBy;

	@NotNull
	private String description;
	
	@NotNull
	private Integer unitCost;
	
	@Column(name="draw_day1")
	@NotNull
	private String drawDay1;
	
	@Column(name="draw_day2")
	private String drawday2;

	@Temporal(TemporalType.DATE)
	@Column(name="update_date")
	private Date updateDate;

	@Column(name="updated_by")
	private String updatedBy;

	//bi-directional many-to-one association to Lottery_Pool
	@OneToMany(mappedBy="lotteryType")
	private Set<LotteryPool> lotteryPools;

	public LotteryType() {
	}

	public int getLotteryTypeId() {
		return this.lotteryTypeId;
	}

	public void setLotteryTypeId(int lotteryTypeId) {
		this.lotteryTypeId = lotteryTypeId;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public Integer getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(Integer unitCost) {
		this.unitCost = unitCost;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getDrawDay1() {
		return drawDay1;
	}

	public void setDrawDay1(String drawDay1) {
		this.drawDay1 = drawDay1;
	}

	public String getDrawday2() {
		return drawday2;
	}

	public void setDrawday2(String drawday2) {
		this.drawday2 = drawday2;
	}

	public Set<LotteryPool> getLotteryPools() {
		return this.lotteryPools;
	}

	public void setLotteryPools(Set<LotteryPool> lotteryPools) {
		this.lotteryPools = lotteryPools;
	}

	public LotteryPool addLotteryPool(LotteryPool lotteryPool) {
		getLotteryPools().add(lotteryPool);
		lotteryPool.setLotteryType(this);

		return lotteryPool;
	}

	public LotteryPool removeLotteryPool(LotteryPool lotteryPool) {
		getLotteryPools().remove(lotteryPool);
		lotteryPool.setLotteryType(null);

		return lotteryPool;
	}

}