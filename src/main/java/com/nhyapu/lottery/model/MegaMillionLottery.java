package com.nhyapu.lottery.model;

public class MegaMillionLottery implements LotteryResponse {

	Long lotteryId;
	int[] numbers = new int[5];
	int megaMillionNumber;

	public MegaMillionLottery(Long lotteryId, int[] numbers, int megaMillionNumber) {
		this.lotteryId = lotteryId;
		this.numbers = numbers;
		this.megaMillionNumber = megaMillionNumber;
	}

	public Long getLotteryId() {
		return lotteryId;
	}

	public void setLotteryId(Long lotteryId) {
		this.lotteryId = lotteryId;
	}

	public int[] getNumbers() {
		return numbers;
	}

	public void setNumbers(int[] numbers) {
		this.numbers = numbers;
	}

	public int getMegaMillionNumber() {
		return megaMillionNumber;
	}

	public void setMegaMillionNumber(int megaMillionNumber) {
		this.megaMillionNumber = megaMillionNumber;
	}

}
