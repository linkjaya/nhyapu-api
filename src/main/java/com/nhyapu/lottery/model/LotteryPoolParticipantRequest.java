package com.nhyapu.lottery.model;

import javax.validation.constraints.NotNull;

public class LotteryPoolParticipantRequest {
	
	@NotNull
	private Long lotteryPoolId;
	
	@NotNull
	private Integer participantTicketCount;

	/**
	 * @return the lotteryPoolId
	 */
	public Long getLotteryPoolId() {
		return lotteryPoolId;
	}

	/**
	 * @param lotteryPoolId the lotteryPoolId to set
	 */
	public void setLotteryPoolId(Long lotteryPoolId) {
		this.lotteryPoolId = lotteryPoolId;
	}

	/**
	 * @return the participantTicketCount
	 */
	public Integer getParticipantTicketCount() {
		return participantTicketCount;
	}

	/**
	 * @param participantTicketCount the participantTicketCount to set
	 */
	public void setParticipantTicketCount(Integer participantTicketCount) {
		this.participantTicketCount = participantTicketCount;
	}
	
	
	
	

}
