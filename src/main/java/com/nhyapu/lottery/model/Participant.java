package com.nhyapu.lottery.model;

public class Participant {

	Long accountId;

	Integer participantTicketCount;

	public Participant(Long accountId, Integer participantTicketCount) {
		this.accountId = accountId;
		this.participantTicketCount = participantTicketCount;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Integer getParticipantTicketCount() {
		return participantTicketCount;
	}

	public void setParticipantTicketCount(Integer participantTicketCount) {
		this.participantTicketCount = participantTicketCount;
	}

}
