package com.nhyapu.lottery.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;


public class LotteryPoolResponse {

	private Long lotteryPoolId;

	private Integer unitCost;

	private Integer lotteryPoolSize;

	private LotteryPoolType lotteryPoolType;

	private Integer participantSize;
	
	private Long drawDate;
	
	@JsonProperty("Lotteries")
	private Set<LotteryResponse> lotteriesResponse;

	public Long getLotteryPoolId() {
		return lotteryPoolId;
	}

	public void setLotteryPoolId(Long lotteryPoolId) {
		this.lotteryPoolId = lotteryPoolId;
	}

	public Integer getUnitCost() {
		return unitCost;
	}

	public void setUnitCost(Integer unitCost) {
		this.unitCost = unitCost;
	}

	public Integer getLotteryPoolSize() {
		return lotteryPoolSize;
	}

	public void setLotteryPoolSize(Integer lotteryPoolSize) {
		this.lotteryPoolSize = lotteryPoolSize;
	}

	public LotteryPoolType getLotteryPoolType() {
		return lotteryPoolType;
	}

	public void setLotteryPoolType(LotteryPoolType lotteryPoolType) {
		this.lotteryPoolType = lotteryPoolType;
	}

	public Integer getParticipantSize() {
		return participantSize;
	}

	public void setParticipantSize(Integer participantSize) {
		this.participantSize = participantSize;
	}

	public Set<LotteryResponse> getLotteriesResponse() {
		return lotteriesResponse;
	}

	public void setLotteriesResponse(Set<LotteryResponse> lotteriesResponse) {
		this.lotteriesResponse = lotteriesResponse;
	}

	public Long getDrawDate() {
		return drawDate;
	}

	public void setDrawDate(Long drawDate) {
		this.drawDate = drawDate;
	}
	



}
