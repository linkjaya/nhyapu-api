package com.nhyapu.lottery.model;

import java.util.Set;

public class LotteryPoolParticipantResponse {

	LotteryPoolResponse lotteryPoolResponse;

	Set<Participant> participants;

	public LotteryPoolResponse getLotteryPoolResponse() {
		return lotteryPoolResponse;
	}

	public void setLotteryPoolResponse(LotteryPoolResponse lotteryPoolResponse) {
		this.lotteryPoolResponse = lotteryPoolResponse;
	}

	public Set<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(Set<Participant> participants) {
		this.participants = participants;
	}

}
