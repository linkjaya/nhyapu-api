package com.nhyapu.lottery.model;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

@Component
public class LotteryPoolRequest {
	
	@NotNull
	private Integer lotteryPoolSize;
	
	@NotNull
	private String lotteryType;
	
	private String drawDate;
	
	@NotNull
    private Integer participantSize;
	
	@NotNull
	String lotteryPoolType;
	
	
	public Integer getLotteryPoolSize() {
		return lotteryPoolSize;
	}
	public void setLotteryPoolSize(Integer lotteryPoolSize) {
		this.lotteryPoolSize = lotteryPoolSize;
	}
	public String getLotteryType() {
		return lotteryType;
	}
	public void setLotteryType(String lotteryType) {
		this.lotteryType = lotteryType;
	}
	public String getDrawDate() {
		return drawDate;
	}
	public void setDrawDate(String drawDate) {
		this.drawDate = drawDate;
	}
	public Integer getParticipantSize() {
		return participantSize;
	}
	public void setParticipantSize(Integer participantSize) {
		this.participantSize = participantSize;
	}
	public String getLotteryPoolType() {
		return lotteryPoolType;
	}
	public void setLotteryPoolType(String lotteryPoolType) {
		this.lotteryPoolType = lotteryPoolType;
	}
	
	

    
    
}
