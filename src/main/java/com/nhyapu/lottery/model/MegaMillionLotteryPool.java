package com.nhyapu.lottery.model;

import java.util.Set;

public class MegaMillionLotteryPool extends LotteryPoolResponse {

	private Set<LotteryResponse> megaMillionNumbers;

	public Set<LotteryResponse> getMegaMillionNumbers() {
		return megaMillionNumbers;
	}

	public void setMegaMillionNumbers(Set<LotteryResponse> megaMillionNumbers) {
		this.megaMillionNumbers = megaMillionNumbers;
	}
	
	

}
