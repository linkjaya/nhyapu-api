package com.nhyapu.lottery.model;

import java.math.BigInteger;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nhyapu.authentication.model.Account;

public class RegistrationResponse {

	private String userName;
	private Long accountId;
	private String firstName;
	private String lastName;
	private BigInteger phone;
	
	
	@JsonIgnore
	private OAuth2AccessToken token;
	
	public RegistrationResponse(Account account, OAuth2AccessToken token)
	{
		this.userName = account.getUserName();
		this.accountId=account.getAccountId();
		this.firstName=account.getFirstName();
		this.lastName=account.getLastName();
		this.phone=account.getPhone();
		this.token = token;
	}

	

	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public Long getAccountId() {
		return accountId;
	}



	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public BigInteger getPhone() {
		return phone;
	}



	public void setPhone(BigInteger phone) {
		this.phone = phone;
	}



	public OAuth2AccessToken getToken() {
		return token;
	}

	public void setToken(OAuth2AccessToken token) {
		this.token = token;
	}

}
