package com.nhyapu.lottery.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.nhyapu.authentication.model.Account;

import java.util.Date;

/**
 * The persistent class for the Lottery_Pool_Participant database table.
 * 
 */
@Entity
@Table(name = "Lottery_Pool_Participant")
@NamedQuery(name = "LotteryPoolParticipant.findAll", query = "SELECT l FROM LotteryPoolParticipant l")
public class LotteryPoolParticipant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "lottery_pool_participant_id")
	private Long lotteryPoolParticipantId;

	@Temporal(TemporalType.DATE)
	@Column(name = "create_date")
	@NotNull
	private Date createDate;

	@Column(name = "created_by")
	@NotNull
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name = "update_date")
	private Date updateDate;

	@Column(name = "updated_by")
	private String updatedBy;

	@Column(name = "participant_ticket_count")
	@NotNull
	private Integer participantTicketCount;

	// bi-directional many-to-one association to Account
	@ManyToOne
	@JoinColumn(name = "account_id")
	@NotNull
	private Account account;

	// bi-directional many-to-one association to Lottery_Pool
	@ManyToOne
	@JoinColumn(name = "lottery_pool_id")
	@NotNull
	private LotteryPool lotteryPool;

	public LotteryPoolParticipant() {
	}

	public Long getLotteryPoolParticipantId() {
		return this.lotteryPoolParticipantId;
	}

	public void setLotteryPoolParticipantId(Long lotteryPoolParticipantId) {
		this.lotteryPoolParticipantId = lotteryPoolParticipantId;
	}

	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdateDate() {
		return this.updateDate;
	}

	public Integer getParticipantTicketCount() {
		return participantTicketCount;
	}

	public void setParticipantTicketCount(Integer participantTicketCount) {
		this.participantTicketCount = participantTicketCount;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public LotteryPool getLotteryPool() {
		return this.lotteryPool;
	}

	public void setLotteryPool(LotteryPool lotteryPool) {
		this.lotteryPool = lotteryPool;
	}

}