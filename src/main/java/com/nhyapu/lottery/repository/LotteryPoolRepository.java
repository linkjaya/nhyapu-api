package com.nhyapu.lottery.repository;

import java.util.Date;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nhyapu.lottery.model.LotteryPool;

/**
 * The LotteryPoolRepository interface is a Spring Data JPA data repository for
 * LotteryPool entities. The LotteryPoolRepository provides all the data access
 * behaviors exposed by <code>JpaRepository</code> and additional custom
 * behaviors may be defined in this interface.
 */
@Repository
public interface LotteryPoolRepository extends JpaRepository<LotteryPool, Long> {

	@Query("SELECT a FROM LotteryPool a WHERE  a.lotteryType.lotteryTypeId = :lotteryTypeId and a.lotteryDrawDate = :nextDrawDate")
	Set<LotteryPool> findByLotteryPoolTypeAndDrawDate(@Param("lotteryTypeId") int lotteryTypeId,  @Param("nextDrawDate") Date nextDrawDate);
	
	


}