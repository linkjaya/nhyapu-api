package com.nhyapu.lottery.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nhyapu.lottery.model.LotteryPoolParticipant;

/**
 * The LotteryPoolParticipantRepository interface is a Spring Data JPA data
 * repository for LotteryPoolParticipant entities. The
 * LotteryPoolParticipantRepository provides all the data access behaviors
 * exposed by <code>JpaRepository</code> and additional custom behaviors may be
 * defined in this interface.
 */
@Repository
public interface LotteryPoolParticipantRepository extends JpaRepository<LotteryPoolParticipant, Long> {

	@Query("SELECT a FROM LotteryPoolParticipant a WHERE  a.account.accountId = :accountId")
	Set<LotteryPoolParticipant> findByAccount(@Param("accountId") Long accountId);

}