package com.nhyapu.lottery.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nhyapu.authentication.model.AccountRole;

/**
 * The AccountRoleRepository interface is a Spring Data JPA data repository for
 * Account entities. The AccountRoleRepository provides all the data access
 * behaviors exposed by <code>JpaRepository</code> and additional custom
 * behaviors may be defined in this interface.
 */
@Repository
public interface AccountRoleRepository extends JpaRepository<AccountRole, Long> {
   
}