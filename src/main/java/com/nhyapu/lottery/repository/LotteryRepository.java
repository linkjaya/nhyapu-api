package com.nhyapu.lottery.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nhyapu.lottery.model.Lottery;

/**
 * The LotteryRepository interface is a Spring Data JPA data repository for
 * Lottery entities. The LotteryRepository provides all the data access
 * behaviors exposed by <code>JpaRepository</code> and additional custom
 * behaviors may be defined in this interface.
 */
@Repository
public interface LotteryRepository extends JpaRepository<Lottery, Long> {

}