package com.nhyapu.lottery.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nhyapu.lottery.model.LotteryType;


@Repository
public interface LotteryTypeRepository extends JpaRepository<LotteryType, Integer>{
	
	@Query("SELECT a FROM LotteryType a WHERE a.description = :description")
	LotteryType findByDescription(@Param("description") String description);

}
