package com.nhyapu.lottery.lotterypool.service;

import java.util.List;

import com.nhyapu.lottery.model.LotteryType;

public interface LotteryTypeService {
	
	LotteryType findByLotteryTypeId(Integer lotteryTypeId);
	LotteryType findByDescription(String description);
	List<LotteryType>findAll();

}
