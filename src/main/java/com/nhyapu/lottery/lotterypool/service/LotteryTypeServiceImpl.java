package com.nhyapu.lottery.lotterypool.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nhyapu.lottery.model.LotteryType;
import com.nhyapu.lottery.repository.LotteryTypeRepository;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class LotteryTypeServiceImpl implements LotteryTypeService{
	
	@Autowired
	private LotteryTypeRepository lotteryTypeRepository;

	@Override
	public LotteryType findByLotteryTypeId(Integer lotteryTypeId) {
		return lotteryTypeRepository.findOne(lotteryTypeId);
	}

	@Override
	public LotteryType findByDescription(String description) {
		return lotteryTypeRepository.findByDescription(description);
	}

	@Override
	public List<LotteryType> findAll() {
		return lotteryTypeRepository.findAll();
	}

}
