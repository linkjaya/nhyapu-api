package com.nhyapu.lottery.lotterypool.service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nhyapu.lottery.lotterypool.helper.LotteryPoolHelper;
import com.nhyapu.lottery.model.Lottery;
import com.nhyapu.lottery.model.LotteryPool;
import com.nhyapu.lottery.model.LotteryPoolParticipant;
import com.nhyapu.lottery.model.LotteryPoolParticipantRequest;
import com.nhyapu.lottery.model.LotteryPoolParticipantResponse;
import com.nhyapu.lottery.model.LotteryPoolRequest;
import com.nhyapu.lottery.model.LotteryPoolResponse;
import com.nhyapu.lottery.model.LotteryPoolType;
import com.nhyapu.lottery.model.LotteryResponse;
import com.nhyapu.lottery.model.MegaMillionLottery;
import com.nhyapu.lottery.model.Participant;

@Service
public class LotteryPoolParticipantServiceImpl implements LotteryPoolParticipantService {

	@Autowired
	LotteryPoolHelper lotteryPoolHelper;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public Set<LotteryPoolParticipantResponse> getUpComingLotteryPools(String lotteryType) {

		Set<LotteryPool> lotteryPools = lotteryPoolHelper.getUpComingLotteyPools(lotteryType);

		Set<LotteryPool> lotteryPoolsWithParticipant = new HashSet<LotteryPool>();
		Set<LotteryPool> lotteryPoolsWithOutParticipant = new HashSet<LotteryPool>();

		lotteryPools.forEach(lotteryPool -> ((lotteryPool.getLotteryPoolParticipants().size() == 0)
				? lotteryPoolsWithParticipant : lotteryPoolsWithOutParticipant).add(lotteryPool));

		Set<LotteryPoolParticipant> lotteryPoolParticipants = lotteryPoolsWithParticipant.stream()
				.flatMap(lotteryPool -> lotteryPool.getLotteryPoolParticipants().stream()).collect(Collectors.toSet());

		for (LotteryPool lotteryPool : lotteryPoolsWithParticipant) {
			log.info("LotteryPoolParticipant: " + lotteryPool.getLotteryPoolId());
		}

		Set<LotteryPoolParticipantResponse> lotteryPoolParticipantResponses = lotteryPoolParticipants.stream().map(
				lotteryPoolParticipant -> convertLotteryPoolToLotteryPoolParticipantResponse(lotteryPoolParticipant))
				.collect(Collectors.toSet());

		Set<LotteryPoolParticipantResponse> lotteryPoolWithOutParticipantsResponse = lotteryPoolsWithParticipant
				.stream().map(lotteryPool -> convertLotteryPoolToLotteryPoolParticipantResponse(lotteryPool))
				.collect(Collectors.toSet());

		lotteryPoolParticipantResponses.addAll(lotteryPoolWithOutParticipantsResponse);

		return lotteryPoolWithOutParticipantsResponse;
	}

	@Override
	public LotteryPoolParticipantResponse createLotteryPool(LotteryPoolRequest lotteryPoolRequest) {

		LotteryPool lotteryPool = lotteryPoolHelper.createLotteryPool(lotteryPoolRequest);

		LotteryPoolParticipantResponse lotteryPoolParticipantResponse = convertLotteryPoolToLotteryPoolParticipantResponse(
				lotteryPool);

		return lotteryPoolParticipantResponse;

	}

	@Override
	public LotteryPoolParticipantResponse createLotteryPoolParticipant(String userName,
			LotteryPoolParticipantRequest lotteryPoolParticipantRequest) {

		LotteryPoolParticipant lotteryPoolParticipant = lotteryPoolHelper.createLotteryPoolParticipant(userName,
				lotteryPoolParticipantRequest);
		return convertLotteryPoolToLotteryPoolParticipantResponse(lotteryPoolParticipant);

	}

	@Override
	public Set<LotteryPoolParticipantResponse> getUpComingLotteryPoolParticipant(String userName) {
		Set<LotteryPoolParticipant> lotteryPoolParticipants = lotteryPoolHelper
				.getUpComingLotteryPoolParticipant(userName);
		return convertLotteryPoolToLotteryPoolParticipantResponse(lotteryPoolParticipants);
	}

	private Set<LotteryPoolParticipantResponse> convertLotteryPoolToLotteryPoolParticipantResponse(
			Set<LotteryPoolParticipant> lotteryPoolParticipants) {

		Set<LotteryPoolParticipantResponse> lotteryPoolParticipantResponses = lotteryPoolParticipants.stream().map(
				lotteryPoolParticipant -> convertLotteryPoolToLotteryPoolParticipantResponse(lotteryPoolParticipant))
				.collect(Collectors.toSet());

		return lotteryPoolParticipantResponses;
	}

	private LotteryPoolParticipantResponse convertLotteryPoolToLotteryPoolParticipantResponse(
			LotteryPoolParticipant lotteryPoolParticipant) {
		LotteryPoolParticipantResponse lotteryPoolParticipantResponse = new LotteryPoolParticipantResponse();
		LotteryPool lotteryPool = lotteryPoolParticipant.getLotteryPool();
		Set<LotteryPoolParticipant> lotteryPoolParticipants = lotteryPool.getLotteryPoolParticipants();
		Set<Participant> participants = convertLotteryPoolParticipantsToParticipants(lotteryPoolParticipants);
		LotteryPoolResponse lotteryPoolResponse = convertLotteryPoolToLotteryPoolResponse(lotteryPool);
		lotteryPoolParticipantResponse.setParticipants(participants);
		lotteryPoolParticipantResponse.setLotteryPoolResponse(lotteryPoolResponse);
		return lotteryPoolParticipantResponse;
	}

	private Set<Participant> convertLotteryPoolParticipantsToParticipants(
			Set<LotteryPoolParticipant> lotteryPoolParticipants) {

		Set<Participant> participants = new HashSet<Participant>();
		if (lotteryPoolParticipants != null && !lotteryPoolParticipants.isEmpty()) {
			participants = lotteryPoolParticipants.stream()
					.map(lotteryPoolParticipant -> new Participant(lotteryPoolParticipant.getAccount().getAccountId(),
							lotteryPoolParticipant.getParticipantTicketCount()))
					.collect(Collectors.toSet());
		}
		return participants;
	}

	private LotteryPoolParticipantResponse convertLotteryPoolToLotteryPoolParticipantResponse(LotteryPool lotteryPool) {

		LotteryPoolParticipantResponse lotteryPoolParticipantResponse = new LotteryPoolParticipantResponse();
		LotteryPoolResponse lotteryPoolResponse = convertLotteryPoolToLotteryPoolResponse(lotteryPool);
		lotteryPoolParticipantResponse.setLotteryPoolResponse(lotteryPoolResponse);
		return lotteryPoolParticipantResponse;
	}

	private LotteryPoolResponse convertLotteryPoolToLotteryPoolResponse(LotteryPool lotteryPool) {
		LotteryPoolResponse lotteryPoolResponse = new LotteryPoolResponse();
		lotteryPoolResponse.setLotteryPoolId(lotteryPool.getLotteryPoolId());
		lotteryPoolResponse.setLotteryPoolSize(lotteryPool.getLotteryPoolSize());
		lotteryPoolResponse.setLotteryPoolType(LotteryPoolType.valueOf(lotteryPool.getLotteryPoolType()));
		lotteryPoolResponse.setParticipantSize(lotteryPool.getParticipantSize());
		lotteryPoolResponse.setUnitCost(lotteryPool.getLotteryType().getUnitCost());
		lotteryPoolResponse.setLotteriesResponse(convertLotteriesToLotteryResponse(lotteryPool.getLotteries()));
		lotteryPoolResponse.setDrawDate(lotteryPool.getLotteryDrawDate().getTime());

		return lotteryPoolResponse;
	}

	private Set<LotteryResponse> convertLotteriesToLotteryResponse(Set<Lottery> lotteries) {
		Set<LotteryResponse> lotteriesResponse = new HashSet<LotteryResponse>();
		lotteriesResponse = lotteries.stream().map(lottery -> new MegaMillionLottery(lottery.getLotteryId(),
				convertLotteryToNumbers(lottery), lottery.getNumber6())).collect(Collectors.toSet());

		return lotteriesResponse;
	}

	private int[] convertLotteryToNumbers(Lottery lottery) {
		int[] numbers = { lottery.getNumber1(), lottery.getNumber2(), lottery.getNumber3(), lottery.getNumber4(),
				lottery.getNumber5() };
		return numbers;
	}

}
