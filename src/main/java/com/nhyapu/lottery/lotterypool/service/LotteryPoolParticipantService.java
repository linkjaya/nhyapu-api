package com.nhyapu.lottery.lotterypool.service;

import java.util.Set;

import com.nhyapu.lottery.model.LotteryPoolParticipantRequest;
import com.nhyapu.lottery.model.LotteryPoolParticipantResponse;
import com.nhyapu.lottery.model.LotteryPoolRequest;

public interface LotteryPoolParticipantService {

	LotteryPoolParticipantResponse createLotteryPool(LotteryPoolRequest lotteryPoolRequest);
	LotteryPoolParticipantResponse createLotteryPoolParticipant(String userName, LotteryPoolParticipantRequest lotteryPoolParticipantRequest);
	Set<LotteryPoolParticipantResponse> getUpComingLotteryPoolParticipant(String userName);
	Set<LotteryPoolParticipantResponse> getUpComingLotteryPools(String lotteryType);
	

}
