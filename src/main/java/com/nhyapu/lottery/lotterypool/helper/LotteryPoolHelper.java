package com.nhyapu.lottery.lotterypool.helper;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nhyapu.authentication.model.Account;
import com.nhyapu.lottery.model.Lottery;
import com.nhyapu.lottery.model.LotteryPool;
import com.nhyapu.lottery.model.LotteryPoolParticipant;
import com.nhyapu.lottery.model.LotteryPoolParticipantRequest;
import com.nhyapu.lottery.model.LotteryPoolRequest;
import com.nhyapu.lottery.model.LotteryType;
import com.nhyapu.lottery.repository.AccountRepository;
import com.nhyapu.lottery.repository.LotteryPoolParticipantRepository;
import com.nhyapu.lottery.repository.LotteryPoolRepository;
import com.nhyapu.lottery.repository.LotteryRepository;
import com.nhyapu.lottery.repository.LotteryTypeRepository;

@Component
public class LotteryPoolHelper {

	@Autowired
	LotteryPoolRepository lotteryPoolRepository;

	@Autowired
	LotteryRepository lotteryRepository;

	@Autowired
	LotteryTypeRepository lotteryTypeRepository;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	LotteryPoolParticipantRepository lotteryPoolParticipantRepository;

	public LotteryPoolParticipant createLotteryPoolParticipant(String userName,
			LotteryPoolParticipantRequest lotteryPoolParticipantRequest) {
		LotteryPoolParticipant lotteryPoolParticipant = new LotteryPoolParticipant();
		Account account = accountRepository.findByUsername(userName);
		LotteryPool lotteryPool = lotteryPoolRepository.findOne(lotteryPoolParticipantRequest.getLotteryPoolId());
		lotteryPoolParticipant.setAccount(account);
		Date currentDate = new Date();
		String createdBy = "ADMIN";
		lotteryPoolParticipant.setCreateDate(currentDate);
		lotteryPoolParticipant.setCreatedBy(createdBy);
		lotteryPoolParticipant.setLotteryPool(lotteryPool);
		lotteryPoolParticipant.setParticipantTicketCount(lotteryPoolParticipantRequest.getParticipantTicketCount());
		return lotteryPoolParticipantRepository.save(lotteryPoolParticipant);
	}

	public LotteryPool createLotteryPool(LotteryPoolRequest lotteryPoolRequest) {
		LotteryPool lotteryPool = new LotteryPool();
		Integer lotteryPoolSize = lotteryPoolRequest.getLotteryPoolSize();
		Date currentDate = new Date();
		String createdBy = "ADMIN";
		LotteryType lotteryType = lotteryTypeRepository.findByDescription(lotteryPoolRequest.getLotteryType());
		lotteryPool.setParticipantSize(lotteryPoolRequest.getParticipantSize());
		lotteryPool.setCreateDate(currentDate);
		lotteryPool.setCreatedBy(createdBy);
		lotteryPool.setLotteryPoolSize(lotteryPoolSize);
		lotteryPool.setLotteryPoolType(lotteryPoolRequest.getLotteryPoolType());
		lotteryPool.setLotteryType(lotteryType);
		lotteryPool.setLotteryDrawDate(nextDrawDate(lotteryType.getDrawDay1(), lotteryType.getDrawday2()));

		lotteryPool = lotteryPoolRepository.save(lotteryPool);

		Lottery lottery;

		for (int i = 0; i < lotteryPoolSize; ++i) {
			lottery = generateRandomLotteryNumbers();
			lottery.setLotteryPool(lotteryPool);
			lottery.setCreateDate(currentDate);
			lottery.setCreatedBy(createdBy);
			lotteryPool.addLottery(lottery);
		}
		lotteryRepository.save(lotteryPool.getLotteries());
		return lotteryPool;
	}

	private Lottery generateRandomLotteryNumbers() {
		int min = 1;
		int max = 75;
		int randomNumber;
		int maxMegaMillionNumber = 15;
		int megaMillionNumber;
		int[] lotteryNumbers = new int[5];
		Lottery lottery = new Lottery();
		Random rand = new Random();
		for (int i = 0; i < 5; ++i) {
			randomNumber = rand.nextInt((max - min) + 1) + min;
			lotteryNumbers[i] = randomNumber;
		}
		Arrays.sort(lotteryNumbers);
		lottery.setNumber1(lotteryNumbers[0]);
		lottery.setNumber2(lotteryNumbers[1]);
		lottery.setNumber3(lotteryNumbers[2]);
		lottery.setNumber4(lotteryNumbers[3]);
		lottery.setNumber5(lotteryNumbers[4]);

		megaMillionNumber = randomNumber = rand.nextInt((maxMegaMillionNumber - min) + 1) + min;
		lottery.setNumber6(megaMillionNumber);

		return lottery;
	}

	private Date nextDrawDate(String drawDay1, String drawDay2) {
		Date nextDrawDate = null;
		LocalDate nextDrawDate1 = LocalDate.now(ZoneId.systemDefault())
				.with(TemporalAdjusters.nextOrSame(DayOfWeek.valueOf(drawDay1)));
		if (drawDay2 != null) {
			LocalDate nextDrawDate2 = LocalDate.now(ZoneId.systemDefault())
					.with(TemporalAdjusters.nextOrSame(DayOfWeek.valueOf(drawDay2)));
			if (nextDrawDate2.compareTo(nextDrawDate1) < 0) {
				nextDrawDate = Date.from(nextDrawDate2.atStartOfDay(ZoneId.systemDefault()).toInstant());
			} else {
				nextDrawDate = Date.from(nextDrawDate1.atStartOfDay(ZoneId.systemDefault()).toInstant());
			}
		} else {
			nextDrawDate = Date.from(nextDrawDate1.atStartOfDay(ZoneId.systemDefault()).toInstant());
		}
		return nextDrawDate;
	}

	public Set<LotteryPoolParticipant> getUpComingLotteryPoolParticipant(String userName) {
		Account account = accountRepository.findByUsername(userName);

		Date startofToday = getStartOfToday();
		Set<LotteryPoolParticipant> lotteryPoolParticipants = lotteryPoolParticipantRepository
				.findByAccount(account.getAccountId());
		Set<LotteryPoolParticipant> subLotteryPoolParticipants = lotteryPoolParticipants.stream()
				.filter(lotteryPoolPaticipant -> lotteryPoolPaticipant.getLotteryPool().getLotteryDrawDate()
						.compareTo(startofToday) >= 0)
				.collect(Collectors.toSet());

		return subLotteryPoolParticipants;
	}

	private Date getStartOfToday() {
		Date currentDate = new Date();
		LocalDateTime currentLocatDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(currentDate.getTime()),
				ZoneId.systemDefault());
		LocalDateTime startOfDay = currentLocatDateTime.with(LocalTime.MIN);
		final Date startOfToday = Date.from(startOfDay.atZone(ZoneId.systemDefault()).toInstant());
		return startOfToday;
	}

	public Set<LotteryPool> getUpComingLotteyPools(String lotteryTypeDescription) {

		LotteryType lotteryType = lotteryTypeRepository.findByDescription(lotteryTypeDescription);
		Date nextDrawDate = nextDrawDate(lotteryType.getDrawDay1(), lotteryType.getDrawday2());

		Set<LotteryPool> lotteryPools = lotteryPoolRepository.findByLotteryPoolTypeAndDrawDate(lotteryType.getLotteryTypeId(),
				nextDrawDate);
		
		return lotteryPools;
		

	}

}
