package com.nhyapu.lottery.controller;

import java.security.Principal;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nhyapu.lottery.lotterypool.service.LotteryPoolParticipantService;
import com.nhyapu.lottery.model.LotteryPoolParticipantRequest;
import com.nhyapu.lottery.model.LotteryPoolParticipantResponse;
import com.nhyapu.lottery.model.LotteryPoolRequest;
import com.nhyapu.settings.errors.InvalidRequestException;

@RestController
public class LotteryPoolController {

	@Autowired
	LotteryPoolParticipantService lotteryPoolParticipantService;

	@RequestMapping(value = "/admin/lotterypool", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LotteryPoolParticipantResponse> createLotteryPool(
			@Valid @RequestBody LotteryPoolRequest lotteryPoolRequest, BindingResult errors, Principal principal) {

		if (errors.hasErrors()) {
			throw new InvalidRequestException("Error in validation of lotteryPoolRequest", errors);
		}

		return new ResponseEntity<LotteryPoolParticipantResponse>(
				lotteryPoolParticipantService.createLotteryPool(lotteryPoolRequest), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/user/lotterypool/participant", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LotteryPoolParticipantResponse> createLotteryPoolParticipant(
			@Valid @RequestBody LotteryPoolParticipantRequest lotteryPoolParticipantRequest, BindingResult errors,
			Principal principal) {

		if (errors.hasErrors()) {
			throw new InvalidRequestException("Error in validation of lotteryPoolParticipantRequest", errors);
		}

		return new ResponseEntity<LotteryPoolParticipantResponse>(lotteryPoolParticipantService
				.createLotteryPoolParticipant(principal.getName(), lotteryPoolParticipantRequest), HttpStatus.CREATED);
	}

	@RequestMapping(value = "/user/lotterypool/upcoming", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Set<LotteryPoolParticipantResponse>> getUpComingLotteryPoolParticipant(Principal principal) {

		Set<LotteryPoolParticipantResponse> lotteryPoolParticipants = lotteryPoolParticipantService
				.getUpComingLotteryPoolParticipant(principal.getName());

		return new ResponseEntity<Set<LotteryPoolParticipantResponse>>(lotteryPoolParticipants, HttpStatus.OK);

	}
	
	@RequestMapping(value = "/all/lotterypool/upcoming", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Set<LotteryPoolParticipantResponse>> getUpComingLotteryPools(@RequestParam String lotteryType) {

		Set<LotteryPoolParticipantResponse> lotteryPoolParticipants = lotteryPoolParticipantService
				.getUpComingLotteryPools(lotteryType);

		return new ResponseEntity<Set<LotteryPoolParticipantResponse>>(lotteryPoolParticipants, HttpStatus.OK);

	}

}
