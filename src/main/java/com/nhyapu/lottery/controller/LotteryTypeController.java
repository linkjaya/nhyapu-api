package com.nhyapu.lottery.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nhyapu.lottery.lotterypool.service.LotteryTypeService;
import com.nhyapu.lottery.model.LotteryType;

public class LotteryTypeController {

	@Autowired
	LotteryTypeService lotteryTypeService;

	@RequestMapping(value = "/lotteryType", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LotteryType> getLotteryType(HttpServletResponse response) {
		return new ResponseEntity<LotteryType>(lotteryTypeService.findByLotteryTypeId(1), HttpStatus.CREATED);
	}

}
