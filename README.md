# README #

APIs 

### What is this repository for? ###

* This repository is for learning Rest Services, mongoDB, Docker, mysql, solr, activemq, swagger, spring-boot etc
* Version: 1.0

### How do I get set up? ###

* **Summary of set up:**
     * Checkout nhyapu-email
     * Download jasypt
     	- ./encrypt.sh input="This is my message to be encrypted" password=your_email_password
     	- update application.properties with email, and encrypted password
     * Build docker image for nhyapu-email (mvn clean compile package docker:build)
     * Checkout the nhyapu-api
     * Build docker image for nhyapu-api (mvn clean compile package docker:build)
     * create encrypt.env under nhyapu-api folder. Entries for encrypt.env
       	ENCRYPTOR_PASSWORD=password_used_to_encrypt_email_password
     * To create and containers and run the application
        - docker-compose up
    
* **Configuration:**
     Required configuration is on docker-compose.yml
* **Dependencies:**
     none
* **Database configuration:
**     mysql server setup and docker container
* **How to run tests:**
          * http://localhost:8888/nhyapu/api/finance/LOW 
* **Deployment instructions:**

   After making code change commit and push
   mvn clean package docker:build docker:push
   to push in .m2/settings.xml, have to add 

        <server>
                <id>docker-hub</id>
                <username>yourUserName</username>
                <password>yourSecretPassword</password>
                <configuration>
                <email>your@email.com</email>
                </configuration>
        </server>



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin : Jaya Pasachhe (linkjaya@gmail.com)
* Other community or team contact