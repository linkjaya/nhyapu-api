DROP DATABASE IF EXISTS lottery;

CREATE DATABASE lottery;

USE lottery;

CREATE TABLE `account` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `credentials_expired` bit(1) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `expired` bit(1) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `locked` bit(1) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` decimal(19,2) NOT NULL,
  `update_date` date DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `UK_f6xpj7h12wr185bqhfi1hqlbr` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `update_date` date DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


CREATE TABLE `account_role` (
  `account_role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` date NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `update_date` date DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `account_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`account_role_id`),
  KEY `FK_ibmw1g5w37bmuh5fc0db7wn10` (`account_id`),
  KEY `FK_p2jpuvn8yll7x96rae4hvw3sj` (`role_id`),
  CONSTRAINT `FK_ibmw1g5w37bmuh5fc0db7wn10` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`),
  CONSTRAINT `FK_p2jpuvn8yll7x96rae4hvw3sj` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `lottery_type` (
  `lottery_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` date NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `draw_day1` varchar(255) NOT NULL,
  `draw_day2` varchar(255) DEFAULT NULL,
  `unit_cost` int(11) NOT NULL,
  `update_date` date DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`lottery_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


CREATE TABLE `lottery_pool` (
  `lottery_pool_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` date NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `lottery_draw_date` date NOT NULL,
  `lottery_pool_size` int(11) NOT NULL,
  `lottery_pool_type` varchar(255) NOT NULL,
  `participant_size` int(11) NOT NULL,
  `update_date` date DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `lottery_type_id` int(11) NOT NULL,
  PRIMARY KEY (`lottery_pool_id`),
  KEY `FK_5qm9pga8tl9a2ri568dhbjrjw` (`lottery_type_id`),
  CONSTRAINT `FK_5qm9pga8tl9a2ri568dhbjrjw` FOREIGN KEY (`lottery_type_id`) REFERENCES `lottery_type` (`lottery_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


CREATE TABLE `lottery` (
  `lottery_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` date NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `number1` int(11) NOT NULL,
  `number10` int(11) DEFAULT NULL,
  `number2` int(11) NOT NULL,
  `number3` int(11) NOT NULL,
  `number4` int(11) NOT NULL,
  `number5` int(11) NOT NULL,
  `number6` int(11) DEFAULT NULL,
  `number7` int(11) DEFAULT NULL,
  `number8` int(11) DEFAULT NULL,
  `number9` int(11) DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `lottery_pool_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`lottery_id`),
  KEY `FK_qjby4sy99oqibkkha85ncix4i` (`lottery_pool_id`),
  CONSTRAINT `FK_qjby4sy99oqibkkha85ncix4i` FOREIGN KEY (`lottery_pool_id`) REFERENCES `lottery_pool` (`lottery_pool_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;


CREATE TABLE `lottery_pool_participant` (
  `lottery_pool_participant_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` date NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `participant_ticket_count` int(11) NOT NULL,
  `update_date` date DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `account_id` bigint(20) NOT NULL,
  `lottery_pool_id` bigint(20) NOT NULL,
  PRIMARY KEY (`lottery_pool_participant_id`),
  KEY `FK_fc1c9qswqihxelwsqrqs29tdy` (`account_id`),
  KEY `FK_j8rcrns3js1d8ee97cdbmolnc` (`lottery_pool_id`),
  CONSTRAINT `FK_fc1c9qswqihxelwsqrqs29tdy` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`),
  CONSTRAINT `FK_j8rcrns3js1d8ee97cdbmolnc` FOREIGN KEY (`lottery_pool_id`) REFERENCES `lottery_pool` (`lottery_pool_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


INSERT INTO `role` (`role_id`, `code`, `label`, `created_by`,`create_date` ) VALUES
(1, 'ROLE_USER', 'User', 'ADMIN', CURDATE());

INSERT INTO `role` (`role_id`, `code`, `label`, `created_by`,`create_date`) VALUES
(2, 'ROLE_ADMIN', 'Admin', 'ADMIN', CURDATE());

INSERT INTO lottery.lottery_type
(lottery_type_id, create_date, created_by, description, unit_cost, draw_day1, draw_day2)
VALUES(1, CURDATE(), 'ADMIN', 'MEGA', 1, 'TUESDAY', 'FRIDAY');

INSERT INTO lottery.lottery_type
(lottery_type_id, create_date, created_by, description, unit_cost,  draw_day1, draw_day2)
VALUES(2, CURDATE(), 'ADMIN', 'POWER', 2, 'WEDNESDAY', 'SATURDAY');

